<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Specialization */

$this->title = 'Update Specialization: ' . $model->SpecializationCode;
$this->params['breadcrumbs'][] = ['label' => 'Specializations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->SpecializationCode, 'url' => ['view', 'id' => $model->SpecializationCode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="specialization-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
