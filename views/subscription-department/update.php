<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionDepartment */

$this->title = 'Update Subscription Department: ' . $model->CodeOfActivity;
$this->params['breadcrumbs'][] = ['label' => 'Subscription Departments', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CodeOfActivity, 'url' => ['view', 'CodeOfActivity' => $model->CodeOfActivity, 'Id' => $model->Id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="subscription-department-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
