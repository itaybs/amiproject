<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubscriptionDepartmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscription Departments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-department-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subscription Department', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'CodeOfActivity',
				'label' => 'Code Of Activity',
				'format' => 'raw',
				'value' => function($model){
					return $model->codeOfActivityItem->Description;
				},
				'filter'=>Html::dropDownList('SubscriptionDepartmentSearch[CodeOfActivity]', $CodeOfActivity, $Specializations, ['class'=>'form-control']),
			],
			
			[
				'attribute' => 'Id',
				'label' => 'Name of member',
				'format' => 'raw',
				'value' => function($model){
					return $model->nameOfMemberItem->Name;
				},
				//'filter'=>Html::dropDownList('SubscriptionDepartmentSearch[Id]', $Id, $AllMembers, ['class'=>'form-control']),
			],
			
			[
				'attribute' => 'Id',
				'label' => 'Id of member',
			],	


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
