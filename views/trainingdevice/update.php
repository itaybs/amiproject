<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Trainingdevice */

$this->title = 'Update Trainingdevice: ' . $model->TrainingDeviceCode;
$this->params['breadcrumbs'][] = ['label' => 'Trainingdevices', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TrainingDeviceCode, 'url' => ['view', 'id' => $model->TrainingDeviceCode]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="trainingdevice-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
