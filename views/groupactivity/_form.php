<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Specialization;

/* @var $this yii\web\View */
/* @var $model app\models\Groupactivity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="groupactivity-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<?= $form->field($model, 'nameOfActivity')->textInput() ?>
	
    <?//= $form->field($model, 'CodeOfActivity')->textInput() ?>
	
	<?= $form->field($model, 'CodeOfActivity')->
				dropDownList(Specialization::getSpecializations()) ?>

    <?= $form->field($model, 'Day')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Time')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
