<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Employee;

/* @var $this yii\web\View */
/* @var $model app\models\Presence */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="presence-form">

    <?php $form = ActiveForm::begin(); ?>

   	<?= $form->field($model, 'EmployeeId')->
				dropDownList(Employee::getEmployees()) ?>

    <?= $form->field($model, 'Date')->textInput() ?>

    <?= $form->field($model, 'TimeOfStart')->textInput() ?>

    <?= $form->field($model, 'TimeOfFinish')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
