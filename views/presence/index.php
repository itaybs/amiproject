<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PresenceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Presences';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="presence-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Presence', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'EmployeeId',
				'label' => 'Name of Employee',
				'format' => 'raw',
				'value' => function($model){
					return $model->employeeEmployeeId->Name;
				},
				//'filter'=>Html::dropDownList('MembersSearch[Type]', $type, $types, ['class'=>'form-control']),
			],
            'EmployeeId',
            'Date',
            'TimeOfStart',
            'TimeOfFinish',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
