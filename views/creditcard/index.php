<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CreditcardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Creditcards';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creditcard-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Creditcard', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NumOfCard',
            'NameOfOwner',
            'Validity',
			[
				'attribute' => 'IdOfMember',
				'label' => 'Name of member',
				'format' => 'raw',
				'value' => function($model){
					return $model->nameOfMemberItem->Name;
				},
				//'filter'=>Html::dropDownList('SubscriptionDepartmentSearch[Id]', $Id, $AllMembers, ['class'=>'form-control']),
			],
            'IdOfMember',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
