<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CreditcardSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="creditcard-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'NumOfCard') ?>

    <?= $form->field($model, 'NameOfOwner') ?>

    <?= $form->field($model, 'Validity') ?>

    <?= $form->field($model, 'IdOfMember') ?>
	 
    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
