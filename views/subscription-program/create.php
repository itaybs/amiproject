<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionProgram */

$this->title = 'Create Subscription Program';
$this->params['breadcrumbs'][] = ['label' => 'Subscription Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-program-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
