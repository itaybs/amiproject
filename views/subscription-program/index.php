<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SubscriptionProgramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscription Programs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-program-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subscription Program', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


			[
				'attribute' => 'NumOfProgram',
				'label' => 'Num Of Program',
				'format' => 'raw',
				'value' => function($model){
					return $model->numOfProgramItem->Description;
				},
				'filter'=>Html::dropDownList('SubscriptionProgramSearch[NumOfProgram]', $NumOfProgram, $AllPrograms, ['class'=>'form-control']),
			],
			
			[
				'attribute' => 'Id',
				'label' => 'Name of member',
				'format' => 'raw',
				'value' => function($model){
					return $model->nameOfMemberItem->Name;
				},
				//'filter'=>Html::dropDownList('SubscriptionDepartmentSearch[CodeOfActivity]', $CodeOfActivity, $Specializations, ['class'=>'form-control']),
			],
			
			[
				'attribute' => 'Id',
				'label' => 'Id of member',
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
