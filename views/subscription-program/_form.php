<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Program;
use app\models\Members;


/* @var $this yii\web\View */
/* @var $model app\models\SubscriptionProgram */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscription-program-form">

    <?php $form = ActiveForm::begin(); ?>

    <?//= $form->field($model, 'NumOfProgram')->textInput() ?>

    <?//= $form->field($model, 'Id')->textInput() ?>
	
	<?= $form->field($model, 'NumOfProgram')->
				dropDownList(Program::getAllPrograms()) ?>
				
	<?= $form->field($model, 'Id')->
				dropDownList(Members::getAllMembers()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
