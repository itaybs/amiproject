<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ShiftCode;
use app\models\Employee;

/* @var $this yii\web\View */
/* @var $model app\models\Shift */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shift-form">

    <?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'NumOfShift')->textInput() ?>
   
   <?//= $form->field($model, 'ShiftCode')->textInput() ?>
	<?= $form->field($model, 'ShiftCode')->
				dropDownList(ShiftCode::getShiftCodes()) ?>
   
   <?= $form->field($model, 'DateOfShift')->textInput() ?>
	<?= $form->field($model, 'EmployeeId')->
				dropDownList(Employee::getEmployees()) ?>

 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
