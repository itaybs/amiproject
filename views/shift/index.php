<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shifts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shift-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Shift', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
				'attribute' => 'ShiftCode',
				'label' => 'Shift Code',
				'format' => 'raw',
				'value' => function($model){
					return $model->shiftCodeItem->descripition;
				},
				'filter'=>Html::dropDownList('ShiftSearch[ShiftCode]', $ShiftCode, $ShiftCodes, ['class'=>'form-control']),
			],
            'DateOfShift',
            'NumOfShift',
			[
				'attribute' => 'EmployeeId',
				'label' => 'Employee name',
				'format' => 'raw',
				'value' => function($model){
					return $model->employeeEmployeeId->Name;
				},
				//'filter'=>Html::dropDownList('ShiftSearch[ShiftCode]', $ShiftCode, $ShiftCodes, ['class'=>'form-control']),
			],
			'EmployeeId',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
