<?php

/* @var $this yii\web\View */

$this->title = 'AMI GYM';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1> AMI TEAM </h1>

        <p class="lead">Stopping technological progress to save money is like stopping your watch to save time.</p>
            <p2>If you are not already logged in, click on the button below</p2>
        <p><a class="btn btn-lg btn-success" href="http://itaybs.myweb.jce.ac.il/amiproject/web/site/login">Get started with AMI GYM</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h3>Gym manager - Itay Basteker</h3>

                <p>To get a tip from Itay Basteker Click on the button</p>

                <p><a class="btn btn-lg btn-info" href="http://saloona.co.il/wowyourbody/2013/02/17/%D7%90%D7%99%D7%9E%D7%95%D7%9F-%D7%9B%D7%95%D7%A9%D7%A8-%D7%9C%D7%91%D7%98%D7%9F-%D7%A9%D7%98%D7%95%D7%97%D7%94-%D7%95%D7%A1%D7%A7%D7%A1%D7%99%D7%AA-%D7%91-6-%D7%93%D7%A7%D7%95%D7%AA-tabata/?ref=blog_tags">TIP 1 &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h3>Professional manager - Aviv Atias</h3>

                <p>To get a tip from Aviv Atias Click on the button.</p>

                <p><a class="btn btn-lg btn-info" href="http://saloona.co.il/wowyourbody/2012/02/14/%D7%90%D7%99%D7%9E%D7%95%D7%9F-%D7%91%D7%A1%D7%99%D7%9E%D7%9F-%D7%94%D7%90%D7%94%D7%91%D7%94-%D7%9C%D7%97%D7%99%D7%98%D7%95%D7%91-%D7%94%D7%96%D7%A8%D7%95%D7%A2%D7%95%D7%AA-%D7%96%D7%A8%D7%95%D7%A2/?ref=blog_tags">TIP 2 &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h3>Clerk - Mor Ben-Abu</h3>

                <p>To get a tip from Mor Ben-Abu Click on the button.</p>

                <p><a class="btn btn-lg btn-info" href="http://saloona.co.il/veredmioni/?p=777">TIP 3 &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
