<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmployeeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'EmployeeId') ?>

    <?= $form->field($model, 'UserId') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'LastName') ?>

    <?= $form->field($model, 'Phone') ?>

    <?php // echo $form->field($model, 'DateOfBirth') ?>

    <?php // echo $form->field($model, 'DateOfStartPosition') ?>

    <?php // echo $form->field($model, 'Specialization') ?>

    <?php // echo $form->field($model, 'JobCode') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
