<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MembersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="members-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'LastName') ?>

    <?= $form->field($model, 'Phone') ?>

    <?= $form->field($model, 'DateOfBirth') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'City') ?>

    <?php // echo $form->field($model, 'Adress') ?>

    <?php // echo $form->field($model, 'Type') ?>

    <?php // echo $form->field($model, 'DateOfStart') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
