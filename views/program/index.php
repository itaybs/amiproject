<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Programs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Program', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NumOfProgram',
			[
				'attribute' => 'TrainingDevice',
				'label' => 'Training Device',
				'format' => 'raw',
				'value' => function($model){
					return $model->trainingdeviceItem->Description;
				},
				'filter'=>Html::dropDownList('ProgramSearch[TrainingDevice]', $TrainingDevice, $Trainingdevices, ['class'=>'form-control']),
			],
			
            'NumOfReturn',
			'Description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
