<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Trainingdevice;

/* @var $this yii\web\View */
/* @var $model app\models\Program */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'NumOfProgram')->textInput() ?>
	 <?= $form->field($model, 'Description')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'TrainingDevice')->textInput() ?>
	<?= $form->field($model, 'TrainingDevice')->
				dropDownList(Trainingdevice::getTrainingdevices()) ?>
    <?= $form->field($model, 'NumOfReturn')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
