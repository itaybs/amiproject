<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Typeofmember */

$this->title = 'Update Typeofmember: ' . $model->Type;
$this->params['breadcrumbs'][] = ['label' => 'Typeofmembers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Type, 'url' => ['view', 'id' => $model->Type]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="typeofmember-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
