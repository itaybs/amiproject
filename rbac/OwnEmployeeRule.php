<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnEmployeeRule extends Rule
{
	public $name = 'ownEmployeeRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['employee']) ? $params['employee']->UserId	== $user : false;
		}
		return false;
	}
}
