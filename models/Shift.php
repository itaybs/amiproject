<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shift".
 *
 * @property integer $ShiftCode
 * @property string $DateOfShift
 * @property integer $NumOfShift
 */
class Shift extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shift';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ShiftCode', 'DateOfShift', 'NumOfShift','EmployeeId'], 'required'],
            [['ShiftCode', 'NumOfShift','EmployeeId'], 'integer'],
            [['DateOfShift'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	public function getEmployeeEmployeeId()
    {
        return $this->hasOne(Employee::className(), ['EmployeeId' => 'EmployeeId']);
    }
	 
		public function getShiftCodeItem()
    {
        return $this->hasOne(ShiftCode::className(), ['shiftId' => 'ShiftCode']);
    }
	 
	 
    public function attributeLabels()
    {
        return [
            'ShiftCode' => 'Shift Code',
            'DateOfShift' => 'Date Of Shift',
            'NumOfShift' => 'Num Of Shift',
			'EmployeeId' => 'Employee Id',
        ];
    }
}
