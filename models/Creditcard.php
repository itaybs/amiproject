<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "creditcard".
 *
 * @property integer $NumOfCard
 * @property string $NameOfOwner
 * @property string $Validity
 * @property integer $IdOfMember
 */
class Creditcard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'credit_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NumOfCard', 'NameOfOwner', 'Validity', 'IdOfMember'], 'required'],
            [['NumOfCard', 'IdOfMember'], 'integer'],
            [['Validity'], 'safe'],
            [['NameOfOwner'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	public function getNameOfMemberItem()
    {
        return $this->hasOne(Members::className(), ['Id' => 'IdOfMember']);
    } 
	 
    public function attributeLabels()
    {
        return [
            'NumOfCard' => 'Num Of Card',
            'NameOfOwner' => 'NameOfOwner',
            'Validity' => 'Validity',
            'IdOfMember' => 'Id Of Member',
        ];
    }
}
