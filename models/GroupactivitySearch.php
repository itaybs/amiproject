<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Groupactivity;

/**
 * GroupactivitySearch represents the model behind the search form about `app\models\Groupactivity`.
 */
class GroupactivitySearch extends Groupactivity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodeOfActivity'], 'integer'],
            [['Day', 'Time','nameOfActivity'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Groupactivity::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		$this->CodeOfActivity == -1 ? $this->CodeOfActivity = null : $this->CodeOfActivity;
		
        // grid filtering conditions
        //$query->andFilterWhere([
           // 'CodeOfActivity' => $this->CodeOfActivity,
           // 'Time' => $this->Time,
			//'nameOfActivity' => $this->nameOfActivity,
			
       // ]);

        $query->andFilterWhere(['like', 'Time',  $this->Time])
		->andFilterWhere(['like', 'Day',  $this->Day])
				->andFilterWhere(['like', 'nameOfActivity', $this->nameOfActivity]);
		

        return $dataProvider;
    }
}
