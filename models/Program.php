<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "program".
 *
 * @property integer $NumOfProgram
 * @property integer $TrainingDevice
 * @property integer $NumOfReturn
 */
class Program extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NumOfProgram', 'TrainingDevice', 'NumOfReturn','Description'], 'required'],
            [['NumOfProgram', 'TrainingDevice', 'NumOfReturn'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	public function getTrainingdeviceItem()
    {
        return $this->hasOne(Trainingdevice::className(), ['TrainingDeviceCode' => 'TrainingDevice']);
    }
	 
	 
	//public function getProgramTrainingDevice()
    //{
    //    return $this->hasOne(Trainingdevice::className(), ['TrainingDeviceCode' => 'Trainingdevice']);
    //}
	 
	 
	 
    public function attributeLabels()
    {
        return [
            'NumOfProgram' => 'Num Of Program',
            'TrainingDevice' => 'Training Device',
            'NumOfReturn' => 'Num Of Return',
			'Description' => 'Description',
        ];
    }
	public static function getAllPrograms()
	{
		$allProgram = self::find()->all();
		$allProgramArray = ArrayHelper::
					map($allProgram, 'NumOfProgram', 'Description');
		return $allProgramArray;						
	}
	
	public static function getAllProgramsWithAllAllPrograms()
	{
		$allAllPrograms = self::getAllPrograms();
		$allAllPrograms[-1] = 'All Programs';
		$allAllPrograms = array_reverse ( $allAllPrograms, true );
		return $allAllPrograms;	
	}	
}
