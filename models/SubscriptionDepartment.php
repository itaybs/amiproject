<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription_department".
 *
 * @property integer $CodeOfActivity
 * @property integer $Id
 */
class SubscriptionDepartment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodeOfActivity', 'Id'], 'required'],
            [['CodeOfActivity', 'Id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	 
	public function getCodeOfActivityItem()
    {
        return $this->hasOne(Specialization::className(), ['SpecializationCode' => 'CodeOfActivity']);
    }
	
	public function getNameOfMemberItem()
    {
        return $this->hasOne(Members::className(), ['Id' => 'Id']);
    }
	 
	 
    public function attributeLabels()
    {
        return [
            'CodeOfActivity' => 'Code Of Activity',
            'Id' => 'Name of member',
        ];
    }
}
