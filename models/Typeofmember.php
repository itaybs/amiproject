<?php

namespace app\models;

	use Yii;
	use \yii\web\IdentityInterface;
	use yii\db\ActiveRecord;
	use yii\behaviors\BlameableBehavior;
	use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "typeofmember".
 *
 * @property integer $Type
 * @property string $Description
 */
class Typeofmember extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_of_member';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Type', 'Description'], 'required'],
            [['Type'], 'integer'],
            [['Description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Type' => 'Type',
            'Description' => 'Description',
        ];
    }
	
		public static function getTypes()
	{
		$allTypes = self::find()->all();
		$allTypesArray = ArrayHelper::
					map($allTypes, 'Type', 'Description');
		return $allTypesArray;						
	}
}
