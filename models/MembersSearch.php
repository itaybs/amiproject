<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Members;

/**
 * MembersSearch represents the model behind the search form about `app\models\Members`.
 */
class MembersSearch extends Members
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'Phone', 'Email', 'Type'], 'integer'],
            [['Name', 'LastName', 'DateOfBirth', 'City', 'Adress', 'DateOfStart'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Members::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        //$query->andFilterWhere([
          //  'Id' => $this->Id,
          //  'Phone' => $this->Phone,
          //  'DateOfBirth' => $this->DateOfBirth,
           // 'Email' => $this->Email,
           // 'Type' => $this->Type,
           // 'DateOfStart' => $this->DateOfStart,
      //  ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'LastName', $this->LastName])
            ->andFilterWhere(['like', 'Id', $this->Id])
			->andFilterWhere(['like', 'Type', $this->Type])
            ->andFilterWhere(['like', 'DateOfBirth', $this->DateOfBirth]);

        return $dataProvider;
    }
}
