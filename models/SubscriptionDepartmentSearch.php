<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubscriptionDepartment;

/**
 * SubscriptionDepartmentSearch represents the model behind the search form about `app\models\SubscriptionDepartment`.
 */
class SubscriptionDepartmentSearch extends SubscriptionDepartment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CodeOfActivity', 'Id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubscriptionDepartment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

		$this->CodeOfActivity == -1 ? $this->CodeOfActivity = null : $this->CodeOfActivity;
		
        // grid filtering conditions
        $query->andFilterWhere([
            'CodeOfActivity' => $this->CodeOfActivity,
            'Id' => $this->Id,
        ]);

        return $dataProvider;
    }
}
