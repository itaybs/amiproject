<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Creditcard;

/**
 * CreditcardSearch represents the model behind the search form about `app\models\Creditcard`.
 */
class CreditcardSearch extends Creditcard
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NumOfCard', 'IdOfMember'], 'integer'],
            [['NameOfOwner', 'Validity'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Creditcard::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
       // $query->andFilterWhere([
         //   'NumOfCard' => $this->NumOfCard,
          //  'Validity' => $this->Validity,
          //  'IdOfMember' => $this->IdOfMember,
        //]);

        $query->andFilterWhere(['like', 'NumOfCard', $this->NumOfCard])
			->andFilterWhere(['like', 'Validity', $this->Validity])
			->andFilterWhere(['like', 'NameOfOwner', $this->NameOfOwner])
            ->andFilterWhere(['like', 'IdOfMember', $this->IdOfMember]);
        return $dataProvider;
    }
}
