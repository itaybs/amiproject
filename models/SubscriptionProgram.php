<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "subscription_program".
 *
 * @property integer $NumOfProgram
 * @property integer $Id
 */
class SubscriptionProgram extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_program';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NumOfProgram', 'Id'], 'required'],
            [['NumOfProgram', 'Id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	public function getNumOfProgramItem()
    {
        return $this->hasOne(Program::className(), ['NumOfProgram' => 'NumOfProgram']);
    }
	
	public function getNameOfMemberItem()
    {
        return $this->hasOne(Members::className(), ['Id' => 'Id']);
    }
	 
    public function attributeLabels()
    {
        return [
            'NumOfProgram' => 'Num Of Program',
            'Id' => 'Name of member',
        ];
    }
}
