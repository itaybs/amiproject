<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "presence".
 *
 * @property integer $EmployeeId
 * @property string $Date
 * @property string $TimeOfStart
 * @property string $TimeOfFinish
 */
class Presence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'presence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['EmployeeId', 'Date', 'TimeOfStart', 'TimeOfFinish'], 'required'],
            [['EmployeeId'], 'integer'],
            [['Date', 'TimeOfStart', 'TimeOfFinish'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
	 
	public function getEmployeeEmployeeId()
    {
        return $this->hasOne(Employee::className(), ['EmployeeId' => 'EmployeeId']);
    }
	
    public function attributeLabels()
    {
        return [
            'EmployeeId' => 'Employee ID',
            'Date' => 'Date',
            'TimeOfStart' => 'Time Of Start',
            'TimeOfFinish' => 'Time Of Finish',
        ];
    }
}
