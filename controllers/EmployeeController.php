<?php

namespace app\controllers;

use Yii;
use app\models\Employee;
use app\models\EmployeeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UnauthorizedHttpException;
use app\models\Specialization;
use app\models\Jobs;
use app\models\User;
/**
 * EmployeeController implements the CRUD actions for Employee model.
 */
class EmployeeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employee models.
     * @return mixed
     */
    public function actionIndex()
    {	
		if (!\Yii::$app->user->can('indexEmployee'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view Employees');
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'Specializations' => Specialization::getSpecializationsWithAllSpecializations(),
			'Specialization' => $searchModel->Specialization,
			'Jobs' => Jobs::getJobsWithAllJobs(),
			'JobCode' => $searchModel->JobCode,
        ]);
    }

    /**
     * Displays a single Employee model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {	
		$model = $this->findModel($id);
		if (!\Yii::$app->user->can('viewEmployee') && 
		    !\Yii::$app->user->can('ownEmployeeRule', ['employee' =>$model]) )
			throw new UnauthorizedHttpException ('Hey, You are not allowed to view Employee'); 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
			$roles = User::getRoles(); 
			return $this->render('view', [
				'model' => $model,
				'roles' => $roles,
            ]);
        }
		//if (!\Yii::$app->user->can('viewEmployee') && 
		   // !\Yii::$app->user->can('ownEmployeeRule',['employee' =>$model]))
			//throw new UnauthorizedHttpException ('Hey,  You are not allowed to view Employee');

			
		//if (!\Yii::$app->user->can('viewEmployee'))
		//	throw new UnauthorizedHttpException ('Hey, You are not allowed to view Employee');
		
       // return $this->render('view', [
         //   'model' => $this->findModel($id),
      //  ]);
    }
    public function actionCreate()
    {
        $model = new Employee();
		if (!\Yii::$app->user->can('createEmployee'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to create Employee');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->EmployeeId]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Employee model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		if (!\Yii::$app->user->can('updateEmployee'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to update Employees ');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->EmployeeId]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Employee model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        
		if (!\Yii::$app->user->can('deleteEmployee'))
			throw new UnauthorizedHttpException ('Hey, You are not allowed to delete Employee');
		$this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Employee model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employee the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
